<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuario')->unique();
            $table->string('nome');
            $table->string('email')->unique();
            $table->string('senha');
            $table->string('cpf')->unique();
            $table->string('cidade');
            $table->string('estado');
            $table->string('celular')
            $table->float('saldo', 10, 2);
            $table->string('dados_banco');
            $table->string('dados_agencia');
            $table->enum('dados_tipoconta', ['poupanca','corrente','picpay']);
            $table->string('dados_conta');
            $table->enum('status', ['pendente','ativo']);
            $table->string('picpay');
            $table->primary('id');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
