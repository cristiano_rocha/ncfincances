<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ URL::to('css/app.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ URL::to('js/custom.js') }}"></script>
         
        
    </head>
    <body>
      @component('components.navbar')
        @slot('title')
          Nacional Cotas
        @endslot
      @endcomponent

      @component('components.sidebar')
      @endcomponent

      @component('components.content')
      @endcomponent
  
     
   
   <script src="{{ URL::to('js/bootstrap.js') }}"></script>     
    </body>
</html>
