
    <div class="content col-xs-12" id="content">

        <ol id="bdcrumb" class="breadcrumb breadcrumb-arrow">
            <li><a href="#">Início</a></li>
            <li class="active"><span>Dados Pessoais</span></li>
        </ol>

        <section id="dados">
            <div class="card py-4 text-center">
                <div class="card-block display-4">Dados Pessoais</div>
            </div>

            <div class="card mt-2">
                <div class="card-block p-2">
                    <form>
                        <div class="form-row row">
                            <div class="form-group col-md-6">
                                <label for="nome" class=" col-form-label">Nome:</label>
                                <input type="text" class="form-control" id="nome" readonly placeholder="Cristiano Rocha">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="cpf" class=" col-form-label">CPF:</label>
                                <input type="text" class="form-control col-form-label" id="cpf" readonly placeholder="999.999.999-99">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="estado" class="col-form-label">Estado:</label>
                                <input type="text"  id="estado" class="form-control" placeholder="Rio Grande do Sul" readonly>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="cidade" class="col-form-label">Cidade:</label>
                                <input type="text"  id="cidade" class="form-control" placeholder="Porto Alegre" readonly>
                            </div>

                             <div class="form-group col-md-6">
                                <label for="ceular" class="col-form-label">Celular:</label>
                                <input type="phone"  id="celular" class="form-control" placeholder="(99) 99999-9999" readonly>
                            </div>

                             <div class="form-group col-md-6">
                                <label for="email" class="col-form-label">Email:</label>
                                <input type="email"  id="email" class="form-control" placeholder="example@mail.com" readonly>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="conta" class="col-form-label">Banco:</label>
                                <input type="text"  id="conta" class="form-control" placeholder="Bradesco" readonly>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="cidade" class="col-form-label">Agência:</label>
                                <input type="email"  id="agencia" class="form-control" placeholder="04919-67" readonly>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="tipo" class="col-form-label">Tipo:</label>
                                <input type="text"  id="tipo" class="form-control" placeholder="Poupança" readonly>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="tipo" class="col-form-label">Conta:</label>
                                <input type="text"  id="tipo" class="form-control" placeholder="Conta" readonly>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>