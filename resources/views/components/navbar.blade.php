 <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse navbar-ml" id="navbar">
          <button class="navbar-toggler navbar-toggler-right navbar-toggle icon-bars" type="button" aria-controls="navbarCollapse" aria-expanded="false" data-toggle="collapse" data-target="#navbarCollapse"><span class="navbar-toggler-icon" aria-label="Toggle Navigation"></span></button>
          <a href="#" class="navbar-brand text-left">{{$title}}</a>
          {{$slot}}
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav">
              <li class="nav-item"><a href="#" class="nav-link">None</a></li>
              <li class="nav-item"><a href="#" class="nav-link">None</a></li>
              <li class="nav-item"><a href="#" class="nav-link">None</a></li>
              <li class="nav-item"><a href="#" class="nav-link">None</a></li>
            </ul>
          </div>
      </nav>
