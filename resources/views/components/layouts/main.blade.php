<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="{{ URL::to('css/main.css') }}" rel="stylesheet" type="text/css">
         
        
    </head>
    <body>
     
        @yield('content')
     
   
   <script src="{{ URL::to('js/bootstrap.js') }}"></script>     
    </body>
</html>
