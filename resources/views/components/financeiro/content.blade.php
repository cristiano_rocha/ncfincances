
    <div class="content" id="content">

        <ol id="bdcrumb" class="breadcrumb breadcrumb-arrow">
            <li><a href="#">Início</a></li>
            <li class="active"><span>Financeiro</span></li>
        </ol>

        <section id="financeiro" class="col-md-12">
            <div class="card py-1 ">
                <div class="card-block col-md-6 col-lg-6" style="font-size: 22px;">Financeiro</div>
            </div>

            <div class="card mx-auto">
                <div class="container">
                    <div class="row">
                        <div class="card-block bg-blue text-white mt-2 mx-1 my-2 p-4" id="card-one">
                            <p class="h5">Cotas Compradas</p>
                            <footer>50</footer>
                        </div>

                        <div class="card-block bg-pink text-white mt-2 mx-1 my-2 p-4">
                            <p class="h5">Total Investido</p>
                            <footer>R$ 500,00</footer>
                        </div>

                        <div class="card-block bg-green text-white mt-2 mx-1 my-2 p-4">
                            <p class="h5">Saldo Disponivel</p>
                            <footer>R$ 250,00</footer>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>