<div class="wrapper">
        <div class=" row row-offcanvas row-offcanvas-left">
            <nav tabindex="0" id="sidebar" class="sidebar-offcanvas">
            <div class="sidebar-header">
                <h3 class="py-3 px-4">Nacional Cotas</h3>
            </div>


{{--  Sidebar Links  --}}
        <ul class="list-unstyled components">
            <li><a href="#"><i class="fa fa-user"></i>Minha Conta</a></li>
            <li><a href="#"><i class="fa fa-money"></i>Financeiro</a></li>
            <li><a href="#"><i class="fa fa-line-chart"></i>Gráfico</a></li>
            <li><a href="#"><i class="fa fa-shopping-cart"></i>Comprar cotas</a></li>
            <li><a href="#"><i class="fa fa-sticky-note"></i>Comprovantes</a></li>
            <li><a href="#"><i class="fa fa-sign-out"></i>Sair</a></li>
        </ul>
    
    </nav>
        </div>
</div>
  