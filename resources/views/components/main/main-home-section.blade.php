<header id="home-section">
    <div class="dark-overlay">
        <div class="home-inner text-white">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1 class="display-4">CNA <strong>Finances</strong></h1>
                        <div class="d-flex flex-row ">
                            <div class="p-4 align-self-start">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="p-4 align-self-end">
                                <strong>70%</strong> de lucros com seus investimentos garantidos.
                            </div>
                        </div>
                        

                        <div class="d-flex flex-row">
                            <div class="p-4 align-self-start"><i class="fa fa-check"></i></div>
                            <div class="p-4 align-self-end">
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. officia, nam exercitationem.
                            </div>
                        </div>
                        

                        <div class="d-flex flex-row">
                            <div class="p-4 align-self-start"><i class="fa fa-check"></i></div>
                                <div class="p-4 align-self-end">
                                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. officia, nam exercitationem.
                                </div>
                        </div>
                        
                    </div>

                    <div class="col-md-4" id="card">
                        <div class="card bg-card-primary card-form text-center">
                            <div class="card-block">
                                <h2>Login</h2>
                                <p>Entre com usuário e senha</p>
                                    <form>
                                        <div class="form-group">
                                            <input type="text" name="usuario" class="form-control form-control-lg" placeholder="Usuário">
                                        </div>

                                        <div class="form-group">
                                            <input type="password" name="senha" class="form-control form-control-lg" placeholder="Senha">
                                        </div>
                                        <input type="submit" id="submit" value="Entrar" class="btn btn-outline-secondary btn-block">
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>