<nav class="navbar navbar-toggleable bg-navbar navbar-inverse  bg-navbar fixed-top ">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" data-toggle="collapse" data-target="#navbarCollapse"><span class="navbar-toggler-icon"></span></button>
            <a href="#" class="navbar-brand text-white">CNA</a>
            <div class="collpase navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="#" class="nav-link">Quem somos?</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">Como funciona</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">BackOffice</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">Contato</a></li>
                </ul>
            </div>
        </div>
</nav>